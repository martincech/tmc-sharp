﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace logs_report
{
    static class HighchartsConstants
    {
        public const string HighchartsGlobalSettinigs = @"
            Highcharts.setOptions({
                global: {
                    timezone: 'Europe/Prague',
                    timezoneOffset: -60,
                }
            });";

        public const string HighchartsMouseEventsFnc = @"
            $('#container').bind('mousemove touchmove touchstart', function (e) {
                var chart, point, i, event;

                for (i = 0; i<Highcharts.charts.length; i = i + 1) {
                    chart = Highcharts.charts[i];
                    event = chart.pointer.normalize(e.originalEvent); // Find coordinates within the chart
                    point = chart.series[0].searchPoint(event, true); // Get the hovered point

                    if (point) {
                        point.highlight(e);
                    }
                }
            });";

        public const string HighchartsPointerResetFnc = @"
            Highcharts.Pointer.prototype.reset = function() {
                return undefined;
            };";

        public const string HighchartsPointHighlightFnc = @"
            Highcharts.Point.prototype.highlight = function(event) {
                event = this.series.chart.pointer.normalize(event);
                this.onMouseOver(); // Show the hover marker
                //this.series.chart.tooltip.refresh(this); // Show the tooltip
                this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
            };";

        public const string HighchartsSyncExtremesFnc = @"
            function syncExtremes(e) {
                var thisChart = this.chart;

                if (e.trigger !== 'syncExtremes') {
                    // Prevent feedback loop
                    Highcharts.each(Highcharts.charts, function(chart) {
                        if (chart !== thisChart) {
                            if (chart.xAxis[0].setExtremes) {
                                // It is null while updating
                                chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, { trigger: 'syncExtremes' });
                            }
                        }
                    });
                }
            }";

        public const string HighchartsAddPrologue = @"
            $('<div class=""chart-small"">')
                .appendTo('#container')
                .highcharts(";

        public const string HighchartsAddEpilogue = @");";
    }

    static class HighchartsHtml
    {
        public const string Content = @"
            <!DOCTYPE html>
            <html lang='en' xmlns='http://www.w3.org/1999/xhtml'>
            <head>
                <meta charset='utf-8' />
                <title></title>
                <script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
                <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.js'></script>
                <script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data.js'></script>
                <script src='https://code.highcharts.com/highcharts.src.js'></script>
                <script src='https://code.highcharts.com/modules/series-label.src.js'></script>
                <script src='https://code.highcharts.com/modules/exporting.js'></script>
                <script src='https://code.highcharts.com/modules/export-data.js'></script>
                <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO' crossorigin='anonymous'>
                <style>
                    body {
                        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
                        font-size: 12px;
                    }

                    .chart {
                        min-width: 400px;
                        max-width: 100%;
                        height: 400px;
                        margin: 0 auto
                    }

                    .chart-small {
                        min-width: 400px;
                        max-width: 100%;
                        height: 300px;
                        margin: 0 auto
                    }

                    .highcharts-data-table table {
                        width: 100%;
                        margin-bottom: 1rem;
                        background-color: transparent;
                    }

                    .highcharts-data-table table th,
                    .highcharts-data-table table td {
                        padding: 0.75rem;
                        vertical-align: top;
                        border-top: 1px solid #dee2e6;
                    }

                    .highcharts-data-table table thead th {
                        vertical-align: bottom;
                        border-bottom: 2px solid #dee2e6;
                    }

                    .highcharts-data-table table tbody + tbody {
                        border-top: 2px solid #dee2e6;
                    }
                </style>
            </head>
            <body>
                <div id = 'container'></div>
                <script src = 'data.js'></script>
            </body>
            </html>";
    }

    class HighchartsChartSettings
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string YAxisName { get; set; }
        public object[] YAxisCategories { get; set; }
    }

    class HighchartsDataSerie
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("data")]
        public object[] Data { get; set; }
    }

    abstract class HighchartsChart
    {
        public abstract void WriteJsObject(StringBuilder sb);
    }

    class HighchartsLineChart : HighchartsChart
    {
        const string TitleObject = @"
            title: {{
                text: '{0}'
            }},";

        const string SubtitleObject = @"
            subtitle: {{
	            text: '{0}'
            }},";

        const string ChartObject = @"
            chart: {
                type: 'line',
                zoomType: 'x'
            },";

        const string LegendObject = @"
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0
            },";

        const string CreditsObject = @"
            credits: {
                enabled: false
            },";

        const string XAxisObject = @"
            xAxis: {
                type: 'datetime',
                title: {
                    text: 'Date'
                },
                crosshair: true,
                events: {
                    setExtremes: syncExtremes
                },
                labels: {
                    rotation: -45,
                    step: null
                },
                dateTimeLabelFormats: {
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e. %b',
                    week: '%e. %b',
                    month: '%b \'%y',
                    year: '%Y'
                }
            },";

        const string YAxisObjectPrologue = @"
            yAxis: {{
                title: {{
                    text: '{0}'
                }}";

        const string YAxisCategories = @"
            , categories: {0}";

        const string YAxisObjectEpilogue = "},";

        const string TooltipObject = @"
		    tooltip: {
                shared: true,
                crosshairs: true
            },";

        const string ResponsiveObject = @"
		    responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            },";

        const string SeriesObject = "series: ";

        readonly HighchartsChartSettings settings;
        readonly IEnumerable<HighchartsDataSerie> dataSeries;

        public HighchartsLineChart(HighchartsChartSettings settings, IEnumerable<HighchartsDataSerie> dataSeries)
        {
            this.settings = settings;
            this.dataSeries = dataSeries;
        }

        public override void WriteJsObject(StringBuilder sb)
        {
            sb.AppendLine("{");
            if (!string.IsNullOrWhiteSpace(settings.Title))
                sb.AppendFormat(TitleObject, settings.Title).AppendLine();
            if (!string.IsNullOrWhiteSpace(settings.Subtitle))
                sb.AppendFormat(SubtitleObject, settings.Subtitle).AppendLine();
            sb.AppendLine(ChartObject);
            sb.AppendLine(LegendObject);
            sb.AppendLine(CreditsObject);
            sb.AppendLine(XAxisObject);
            sb.AppendFormat(YAxisObjectPrologue, string.IsNullOrWhiteSpace(settings.YAxisName) ? "null" : settings.YAxisName);
            if (settings.YAxisCategories != null)
                sb.AppendFormat(YAxisCategories, JsonConvert.SerializeObject(settings.YAxisCategories)).AppendLine();
            sb.AppendLine(YAxisObjectEpilogue);
            sb.AppendLine(TooltipObject);
            //sb.AppendLine(ResponsiveObject);
            sb.Append(SeriesObject).Append(JsonConvert.SerializeObject(dataSeries)).AppendLine();
            sb.AppendLine("}");
        }
    }

    class JsAnonFunction : IDisposable
    {
        bool finished;
        readonly StringBuilder sb;

        public JsAnonFunction(StringBuilder sb)
        {
            this.sb = sb;
            sb.AppendLine("$(function () {");
        }

        public void UnsafeWriteRaw(string content)
        {
            sb.AppendLine(content);
        }

        public void AddChart(HighchartsChart chart)
        {
            if (chart != null)
            {
                sb.AppendLine(HighchartsConstants.HighchartsAddPrologue);
                chart.WriteJsObject(sb);
                sb.AppendLine(HighchartsConstants.HighchartsAddEpilogue);
            }
        }

        void Dispose(bool disposing)
        {
            if (disposing)
            {
                finished = true;
                sb.AppendLine("});");
            }
        }

        public void Dispose()
        {
            Dispose(!finished);
            GC.SuppressFinalize(this);
        }
    }
}
