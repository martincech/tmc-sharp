﻿namespace logs_report
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Veit.Tmc.Data;

    class Report
    {
        public static void Generate(string outputDir, IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            GenerateHtmlFile(outputDir);
            GenerateJsFile(outputDir, rasEntries);
        }

        static void GenerateHtmlFile(string outputDir)
        {
            File.WriteAllText(Path.Combine(outputDir, "index.html"), HighchartsHtml.Content);
        }

        static void GenerateJsFile(string outputDir, IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var sb = new StringBuilder(0x100000);
            using (var jsanon = new JsAnonFunction(sb))
            {
                jsanon.UnsafeWriteRaw(HighchartsConstants.HighchartsGlobalSettinigs);
                jsanon.UnsafeWriteRaw(HighchartsConstants.HighchartsMouseEventsFnc);
                jsanon.UnsafeWriteRaw(HighchartsConstants.HighchartsPointerResetFnc);
                jsanon.UnsafeWriteRaw(HighchartsConstants.HighchartsPointHighlightFnc);
                jsanon.UnsafeWriteRaw(HighchartsConstants.HighchartsSyncExtremesFnc);

                jsanon.AddChart(CreateStateChart(rasEntries));
                jsanon.AddChart(CreateFailuresChart(rasEntries));
                jsanon.AddChart(CreateDieselTempChart(rasEntries));
                jsanon.AddChart(CreateAccuVoltageChart(rasEntries));
                jsanon.AddChart(CreatePressureChart(rasEntries));
                jsanon.AddChart(CreateCo2Chart(rasEntries));
                jsanon.AddChart(CreateTemperaturesChart(rasEntries));
                jsanon.AddChart(CreateFlapsChart(rasEntries));

            }
            File.WriteAllText(Path.Combine(outputDir, "data.js"), sb.ToString());
        }

        class DataSerieDefinition<T>
        {
            public string Name { get; set; }
            public Func<RasData_V4_5, bool> ShouldIncludeFnc { get; set; } = (rd) => true;
            public Func<IEnumerable<RasData_V4_5>, bool> ShouldIncludeOnSetFnc { get; set; } = null;
            public Func<RasData_V4_5, T> GetValueFnc { get; set; }
        }


        class DataSeriesGenerator
        {
            public static IEnumerable<HighchartsDataSerie> Generate<T>(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries, IEnumerable<DataSerieDefinition<T>> series)
            {
                var result = new List<HighchartsDataSerie>();
                if (rasEntries.Count > 0)
                {
                    var firstRas = rasEntries.First().Value;
                    foreach (var serieDef in series)
                    {
                        if ((serieDef.ShouldIncludeOnSetFnc != null && serieDef.ShouldIncludeOnSetFnc(rasEntries.Values)) ||
                            (serieDef.ShouldIncludeOnSetFnc is null && serieDef.ShouldIncludeFnc(firstRas)))
                        {
                            var data = new List<object>();
                            foreach (var entry in rasEntries.OrderBy(kv => kv.Key))
                            {
                                data.Add(new object[] { entry.Key.ToUnixTimeMilliseconds(), serieDef.GetValueFnc(entry.Value) });
                            }
                            AddGaps(data);
                            result.Add(new HighchartsDataSerie { Name = serieDef.Name, Data = data.ToArray() });
                        }
                    }
                }
                return result;
            }

            const long maxGapTime = 15 * 60 * 1000;
            static void AddGaps(List<object> data)
            {
                for (var i = data.Count - 1; i > 0; --i)
                {
                    var t1 = (long)(data[i] as object[])[0];
                    var t0 = (long)(data[i - 1] as object[])[0];
                    if ((t1 - t0) > maxGapTime)
                        data.Insert(i, new object[] { t0 + 1, null });
                }
            }
        }

        static readonly DataSerieDefinition<int>[] stateChartSeries = new DataSerieDefinition<int>[]
        {
            new DataSerieDefinition<int> {
                Name = "D+",
                GetValueFnc = (rd) => rd.IsTruckEngineRunning ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Diesel Running",
                ShouldIncludeFnc = (rd) => rd.IsDieselEngineInstalled,
                GetValueFnc = (rd) => rd.IsDieselEngineRunning ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Cooling",
                GetValueFnc = (rd) => rd.IsCoolingOn ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Ventilation",
                GetValueFnc = (rd) => rd.IsFanOn ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Heating",
                GetValueFnc = (rd) => rd.HeatingPercent > 0 ? 1 : 0
            }
        };

        static HighchartsChart CreateStateChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings {
                Title = "Subsystems operation",
                YAxisName = "Signaled",
                YAxisCategories = new object[] { 0, 1 }
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, stateChartSeries));
        }

        static readonly DataSerieDefinition<int>[] failureChartSeries = new DataSerieDefinition<int>[]
        {
            new DataSerieDefinition<int> {
                Name = "Fan",
                GetValueFnc = (rd) => rd.FanFailure ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Charging",
                GetValueFnc = (rd) => rd.ChargingFailure ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "CO2",
                GetValueFnc = (rd) => rd.Co2Failure ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Heating",
                GetValueFnc = (rd) => rd.HeatingFailure ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Heating Servo",
                GetValueFnc = (rd) => rd.HeatingServoFailure ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Alarm Shown",
                GetValueFnc = (rd) => rd.IsAlarmShown ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Cooling",
                GetValueFnc = (rd) => rd.CoolingFault ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Front Thermometer",
                GetValueFnc = (rd) => rd.FrontThermometerFault ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Middle Thermometer",
                GetValueFnc = (rd) => rd.MiddleThermometerFault ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Rear Thermometer",
                GetValueFnc = (rd) => rd.RearThermometerFault ? 1 : 0
            },
             new DataSerieDefinition<int> {
                Name = "Motor Overheated",
                ShouldIncludeFnc = (rd) => rd.IsDieselEngineInstalled,
                GetValueFnc = (rd) => rd.IsDieselEngineOverheated ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "ECU Error",
                ShouldIncludeFnc = (rd) => rd.IsDieselEngineInstalled,
                GetValueFnc = (rd) => rd.EcuError ? 1 : 0
            },
            new DataSerieDefinition<int> {
                Name = "Accu Low Voltage",
                GetValueFnc = (rd) => rd.AccumulatorLowVoltage ? 1 : 0
            }
        };

        static HighchartsChart CreateFailuresChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "System Failures",
                YAxisName = "Signaled",
                YAxisCategories = new object[] { 0, 1 }
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, failureChartSeries));
        }

        static readonly DataSerieDefinition<int>[] dieselTempChartSeries = new DataSerieDefinition<int>[]
        {
            new DataSerieDefinition<int> {
                Name = "Temperature",
                GetValueFnc = (rd) => rd.DieselEngineTempereature
            }
        };

        static HighchartsChart CreateDieselTempChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            if (rasEntries.Count > 0 && rasEntries.First().Value.IsDieselEngineInstalled)
            {
                var settings = new HighchartsChartSettings
                {
                    Title = "Diesel Temp",
                    YAxisName = "1/16"
                };
                return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, dieselTempChartSeries));
            }

            return null;
        }

        static readonly DataSerieDefinition<int>[] accuVoltageChartSeries = new DataSerieDefinition<int>[]
        {
            new DataSerieDefinition<int> {
                Name = "Accumulator",
                GetValueFnc = (rd) => rd.AccuVoltage
            }
        };

        static HighchartsChart CreateAccuVoltageChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "Accumulator",
                YAxisName = "Volts"
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, accuVoltageChartSeries));
        }

        static readonly DataSerieDefinition<double>[] pressureChartSeries = new DataSerieDefinition<double>[]
        {
            new DataSerieDefinition<double> {
                Name = "Suction",
                GetValueFnc = (rd) => rd.SuctionPressure
            },
            new DataSerieDefinition<double> {
                Name = "Discharge",
                GetValueFnc = (rd) => rd.DischargePressure
            }
        };

        static HighchartsChart CreatePressureChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "Pressure",
                YAxisName = "Pa"
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, pressureChartSeries));
        }

        static readonly DataSerieDefinition<double>[] co2ChartSeries = new DataSerieDefinition<double>[]
        {
            new DataSerieDefinition<double> {
                Name = "CO2",
                GetValueFnc = (rd) => rd.Co2Concentration
            }
        };

        static HighchartsChart CreateCo2Chart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "Co2 Concentration",
                YAxisName = "ppm"
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, co2ChartSeries));
        }

        static double ToCelsius(double degrees, RasData_V4_5 rd)
        {
            if (rd.TempUnits == TemperatureUnits.Farenheit)
                return (degrees - 32) * 5 / 9;
            return degrees;
        }

        static readonly DataSerieDefinition<double>[] tempChartSeries = new DataSerieDefinition<double>[]
        {
            new DataSerieDefinition<double> {
                Name = "Target",
                GetValueFnc = (rd) => ToCelsius(rd.TargetTemperature, rd)
            },
            new DataSerieDefinition<double> {
                Name = "Front",
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Front)
            },
            new DataSerieDefinition<double> {
                Name = "Middle",
                ShouldIncludeOnSetFnc = (rda) => rda.All(rd => rd.GetTemperature(ThermoAddress.Middle) != -100),
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Middle)
            },
            new DataSerieDefinition<double> {
                Name = "Rear",
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Rear)
            },
            new DataSerieDefinition<double> {
                Name = "Channel",
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Channel)
            },
            new DataSerieDefinition<double> {
                Name = "Outside",
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Outside)
            },
            new DataSerieDefinition<double> {
                Name = "Recirculation",
                GetValueFnc = (rd) => rd.GetTemperature(ThermoAddress.Recirculation)
            }
        };

        static HighchartsChart CreateTemperaturesChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "Temperatures",
                YAxisName = "C"
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, tempChartSeries));
        }

        static readonly DataSerieDefinition<double>[] flapsChartSeries = new DataSerieDefinition<double>[]
        {
            new DataSerieDefinition<double> {
                Name = "Floor Flap Position",
                ShouldIncludeFnc = (rd) => true,
                GetValueFnc = (rd) => rd.FloorFlapPosition
            },
            new DataSerieDefinition<double> {
                Name = "Fresh Air Position",
                ShouldIncludeFnc = (rd) => true,
                GetValueFnc = (rd) => rd.FreshAirPosition
            },
            new DataSerieDefinition<double> {
                Name = "Heating Position",
                ShouldIncludeFnc = (rd) => true,
                GetValueFnc = (rd) => rd.HeatingPosition
            }
        };

        static HighchartsChart CreateFlapsChart(IDictionary<DateTimeOffset, RasData_V4_5> rasEntries)
        {
            var settings = new HighchartsChartSettings
            {
                Title = "Flap positions",
                YAxisName = "1/16"
            };
            return new HighchartsLineChart(settings, DataSeriesGenerator.Generate(rasEntries, flapsChartSeries));
        }
    }
}
