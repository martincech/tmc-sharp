﻿namespace logs_report
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using SimpleBase;
    using Veit.Tmc.Data;

    public static class TmcRasData
    {
        const string magic = "773400340077";
        static readonly int dataLen = Marshal.SizeOf<RasData_V4_5>();
        public static byte[] GetRasDataEntry(DataRoot root)
        {
            var pos = root.Debug.IndexOf(magic);
            if (pos != -1)
            {
                var sdata = root.Debug.Substring(pos + magic.Length, dataLen * 2);
                return Base16.Decode(sdata).ToArray();
            }
            return null;
        }

        public static IDictionary<DateTimeOffset, RasData_V4_5> Parse(IDictionary<DateTimeOffset, DataRoot> source)
        {
            var result = new ConcurrentDictionary<DateTimeOffset, RasData_V4_5>();
#if DEBUG
            source.ForEach(kv =>
#else
            source.AsParallel().ForAll(kv =>
#endif
            {
                var data = GetRasDataEntry(kv.Value);
                if (data != null)
                {
                    // prefer TMC internal time instead of server received, sometimes happens in batches
                    var ras = data.ToArray().BufferToStructure<RasData_V4_5>();
                    // FIXME: ignore data from dumb range
                    //if (Math.Abs((kv.Key - ras.TransferTime).TotalDays) < 2)
                        result.GetOrAdd(ras.TransferTime, ras);
                }
            });
            return result;
        }
    }
}
