﻿namespace logs_report
{
    using System;
    using System.IO;
    using System.Reflection;
    using NDesk.Options;

    class Program
    {
        static readonly string appName = Assembly.GetEntryAssembly().GetName().Name;

        static void Main(string[] args)
        {
            var showHelp = false;
            var dataDir = "data";
            var outputDir = ".";

            var p = new OptionSet
            {
                { "d|data=", () => "Path to directory with log files to analyze.", v => dataDir = v },
                { "o|output=", () => "Path to directory where to place report.", v => outputDir = v },
                { "h|help", () => "Shows this help", v => showHelp = v != null }
            };
            p.Parse(args);

            if (string.IsNullOrEmpty(dataDir) && string.IsNullOrEmpty(outputDir))
            {
                showHelp = true;
            }

            string err = null;

            if (!Directory.Exists(dataDir))
            {
                err += $"Data directory '{dataDir}' does not exist.{Environment.NewLine}";
            }
            if (!Directory.Exists(outputDir))
            {
                err += $"Output directory '{outputDir}' does not exist.{Environment.NewLine}";
            }

            Console.WriteLine(appName);
            Console.WriteLine();
            if (showHelp || err != null)
            {
                p.WriteOptionDescriptions(Console.Out);
                Console.WriteLine();
                Console.Error.Write(err);
                return;
            }

            var logsData = EurosatLogFile.ParseDirectory(dataDir);
            var rasEntries = TmcRasData.Parse(logsData);
            Report.Generate(outputDir, rasEntries);

            Console.WriteLine("Report generated.");
        }
    }
}
