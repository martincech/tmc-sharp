﻿namespace logs_report
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;

    public class DataRel
    {
        [XmlElement("SEND_TIME")]
        public string sendTime;

        [XmlAttribute("server_gmt")]
        public string serverTime;

        [XmlIgnore]
        public DateTimeOffset SendTime => sendTime != null ?
            DateTimeOffset.FromUnixTimeSeconds(long.Parse(sendTime)) :
            DateTimeOffset.FromUnixTimeSeconds(long.Parse(serverTime));

    }

    [XmlRoot("ROOT")]
    public class DataRoot
    {
        [XmlElement("DEBUG")]
        public string Debug { get; set; }

        [XmlElement("REL")]
        public DataRel Rel { get; set; }
    }

    class EurosatLogFile
    {
        static string NormalizeContentNext(string data, ref int pos)
        {
            if (pos >= data.Length)
                return null;

            var nextPos = data.IndexOf("<DEBUG>", pos + 1);
            string subdata;
            if (nextPos != -1)
            {
                subdata = data.Substring(pos, nextPos - pos);
                pos = nextPos;
            }
            else
            {
                subdata = data.Substring(pos);
                pos = data.Length;
            }
            subdata = subdata.Replace("<DEBUG>", "<DEBUG><![CDATA[").Replace("</DEBUG>", "]]></DEBUG>");
            return "<ROOT>" + subdata + "</ROOT>";
        }

        static string TrimBeginning(string originalData)
        {
            var pos = originalData.IndexOf("<DEBUG>");
            return originalData.Substring(pos);
        }

        static readonly XmlSerializer sxml = new XmlSerializer(typeof(DataRoot));
        public static IEnumerable<DataRoot> Parse(string filename)
        {
            string content = null;
            using (var fs = File.OpenRead(filename))
            using (var sr = new StreamReader(fs))
            {
                content = sr.ReadToEnd();
            }
            content = TrimBeginning(content);
            var pos = 0;

            string part;
            while ((part = NormalizeContentNext(content, ref pos)) != null)
            {
                using (var sr = new StringReader(part))
                    yield return (DataRoot)sxml.Deserialize(sr);
            }
        }

        public static IDictionary<DateTimeOffset, DataRoot> ParseDirectory(string directory)
        {
            var result = new ConcurrentDictionary<DateTimeOffset, DataRoot>();

            Directory.EnumerateFiles(directory, "*.bin").AsParallel().ForAll(file =>
            {
                if (!file.Contains(".part."))
                    foreach (var root in Parse(file))
                        result.GetOrAdd(root.Rel.SendTime, root);
            });

            return result;
        }
    }
}
