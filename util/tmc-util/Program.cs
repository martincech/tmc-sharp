﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NDesk.Options;
using Veit.Tmc.Protocol;
using Veit.Tmc.Data;
using System.Runtime.InteropServices;
using SimpleBase;

namespace tmc_util
{
    class Program
    {
        static readonly string appName = Assembly.GetEntryAssembly().GetName().Name;

        static void Main(string[] args)
        {
            Console.WriteLine(appName);
            Console.WriteLine();

            var showHelp = args.Length == 0;
            string checksumData = null;
            string ttime = null;
            var p = new OptionSet
            {
                { "ch|checksum=", () => "Calculate checksum for provided data.", v => checksumData = v },
                { "tt|transfertime=", () => "Print TransferTime in human readable form.", v => ttime = v },
                { "h|help", () => "Shows this help", v => showHelp = v != null }
            };
            p.Parse(args);

            var errors = new List<string>();

            if (!string.IsNullOrEmpty(checksumData))
            {
                if (checksumData.Length % 2 == 0)
                {
                    Console.WriteLine($"Checksum: {Checksum.Calculate(checksumData)}");
                }
                else
                {
                    errors.Add($"ERROR: -ch={checksumData}");
                    errors.Add("Checksum string must be odd length.");
                }
            }
            if (!string.IsNullOrEmpty(ttime))
            {
                if (ttime.Length == 8)
                {
                    DateTime dt = Base16.Decode(ttime).ToArray().BufferToStructure<TransferTime>();
                    Console.WriteLine($"TrasferTime: {dt.ToString("o")}");
                }
                else
                {
                    errors.Add($"ERROR: -tt={ttime}");
                    errors.Add("\tTransfer time must be 4bytes long (hex16, 8 hex digits).");
                }
            }

            if (showHelp)
            {
                p.WriteOptionDescriptions(Console.Out);
            }

            if (errors.Count > 0)
            {
                errors.ForEach(e => Console.Error.WriteLine(e));
            }
        }
    }
}
