﻿using System;
using SimpleBase;

namespace Veit.Tmc.Protocol
{
    public static class Checksum
    {
        public static byte Calculate(ReadOnlySpan<byte> data)
        {
            unchecked
            {
                byte result = 0;
                foreach (var b in data)
                    result += b;
                return (byte)~result;
            }
        }

        public static string Calculate(string sdata)
        {
            var data = Base16.Decode(sdata);
            var res = Calculate(data);
            return Base16.EncodeUpper(new byte[] { res });
        }
    }
}
