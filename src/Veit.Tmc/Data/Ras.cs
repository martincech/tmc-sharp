﻿namespace Veit.Tmc.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    static class Constants
    {
        public const int DutyCycleLSB = 10;
        // Teplota vody na vstupu Webasta 0-100C pro uklakladani tak, aby 100C vyslo na 6 bitu (0-64, tj. 100/2=50)
        public const int HeatingWaterTempLSB = 2;
        // Relativni vlhkost 0-100% pro ukladani tak, aby 100% vyslo na 6 bitu
        public const int HumidityLSB = 2;
        public const int AirPressureLSB = 5;
        public const int Co2LSB = 100;
    }

    // Datum a cas maximalne zkraceny na 4 bajty pro prenos do serveru
    [StructLayout(LayoutKind.Explicit)]
    public struct TransferTime
    {
        [FieldOffset(0)]
        byte b1;

        [FieldOffset(1)]
        byte b2;

        [FieldOffset(2)]
        byte b3;

        [FieldOffset(3)]
        byte b4;

        // subsequent 4 bits of first ushort
        public int Month => (b1 & 0xF0) >> 4;

        // 12 bits of first ushort
        public int Year => ((b1 & 0x0F) << 8) + b2;

        // subsequent 6 bits of second ushort
        public int Min => (b3 & 0xFC) >> 2;

        // subsequent 5 bits of second ushort
        public int Hour => ((b3 & 0x03) << 3) + ((b4 & 0xE0) >> 5);

        // 5 bits of second ushort
        public int Day => (b4 & 0x1F);

        public static implicit operator DateTime(TransferTime t) => new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Min, 0);
    };

    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct RasData_V4_5
    {
        [FieldOffset(0)]
        public byte VersionMajor;

        [FieldOffset(1)]
        public byte VersionMinor;

        [FieldOffset(2)]
        byte byte_02;

        // TLocalizationStatus: lozeni prepravek ve skrini : 3;
        public TrollyLayout CurrentTrollyLayout => (TrollyLayout)((byte_02 & 0x70) >> 5);

        // Napeti ve voltech, : 5,
        public byte AccuVoltage => (byte)(byte_02 & 0x1F);

        [FieldOffset(3)]
        byte byte_03;

        //  : 1;    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)
        public bool RearThermometerFault => (byte_03 & 0x80) != 0;

        // : 1,    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)
        public bool MiddleThermometerFault => (byte_03 & 0x40) != 0;

        // : 1,    // Porucha predniho cidla (nedojeti do pasma cilove teploty)
        public bool FrontThermometerFault => (byte_03 & 0x20) != 0;

        // : 1,    // Nouzove ovladani v predchozim kroku
        public bool OldEmergencyMode => (byte_03 & 0x10) != 0;

        // : 1,    // YES/NO nouzove ovladani z rozvadece
        public bool IsEmergencyModeEnabled => (byte_03 & 0x08) != 0;

        // : 1,    // Cinnost v automatickem rezimu - bud se topi, nebo chladi TAutoMode
        public AutoMode AutoMode => (AutoMode)((byte_03 & 0x04) >> 2);

        // : 2,    // Rezim, ve kterem regulator pracuje TControlMode
        public ControlMode ControlMode => (ControlMode)(byte_03 & 0x03);


        [FieldOffset(4)]
        byte byte_04;

        // : 4;    // Teplota motoru (0-10)
        public byte DieselEngineTempereature => (byte)((byte_04 & 0xF0) >> 4);

        // : 1,    // YES/NO bezi diesel
        public bool IsDieselEngineRunning => (byte_04 & 0x08) != 0;

        // : 1,    // YES/NO zda se diesel motor pouziva
        public bool IsDieselEngineInstalled => (byte_04 & 0x04) != 0;

        // : 1,    // YES/NO porucha chlazeni
        public bool CoolingFault => (byte_04 & 0x02) != 0;

        // : 1,    // YES/NO prave sepnute chlazeni
        public bool IsCoolingOn => (byte_04 & 0x01) != 0;


        [FieldOffset(5)]
        byte byte_05;

        // : 1;    // YES/NO porucha serva topeni
        public bool HeatingServoFailure => (byte_05 & 0x80) != 0;

        // : 1,    // YES/NO porucha samotneho topeni
        public bool HeatingFailure => (byte_05 & 0x40) != 0;

        // : 1,    // YES/NO zapnuty elektromotor
        public bool IsElectroMotorEnabled => (byte_05 & 0x20) != 0;

        // : 1,    // YES/NO prehraty diesel
        public bool IsDieselEngineOverheated => (byte_05 & 0x10) != 0;

        // : 1,    // YES/NO ma se vymenit olej s filtrem
        public bool IsDieselEngineOilChangeRequired => (byte_05 & 0x08) != 0;

        // : 3,    // Stav serva v podlaze
        public ServoStatus ServoStatus => (ServoStatus)(byte_05 & 0x07);


        [FieldOffset(6)]
        byte byte_06;

        // : 7;    // Aktualni hodnota cersrtveho vzduchu v procentech
        public byte ActualFreshAir => (byte)((byte_06 & 0x7F) >> 1);

        //  : 1,    // Typ rizeni TFreshAirMode
        public FreshAirMode FreshAirMode => (FreshAirMode)(byte_06 & 0x01);

        [FieldOffset(7)]
        byte byte_07;

        // : 4, Poloha klapky pro zobrazeni a logovani - 16 kroku
        public byte FloorFlapPosition => (byte)((byte_07 & 0xF0) >> 4);

        // :4, Poloha klapky pro zobrazeni a logovani - 16 kroku
        public byte FreshAirPosition => (byte)(byte_07 & 0x0F);



        [FieldOffset(8)]
        byte byte_08;

        // : 7;   // Aktualni hodnota otevreni klapky v procentech
        public byte ActualFloorFlap => (byte)((byte_08 & 0x7F) >> 1);

        // : 1,   // Typ rizeni
        public FlapMode FlapMode => (FlapMode)(byte_08 & 0x01);

        [FieldOffset(9)]
        byte byte_09;

        // : 1;    // YES/NO topeni 1 prave hori
        public bool IsHeatingFlame1On => (byte_09 & 0x80) != 0;

        // : 3,    // Stav serva recirkulace
        public ServoStatus RecirculationServoStatus => (ServoStatus)((byte_09 & 0x70) >> 4);

        // :4, Poloha serva topeni pro logovani - 16 kroku
        public byte HeatingPosition => (byte)(byte_09 & 0x0F);


        [FieldOffset(10)]
        byte byte_10;

        // : 1,    YES/NO zapnute ventilatory
        public bool IsFanOn => (byte_10 & 0x80) != 0;

        // : 7;    Aktualni hodnota vykonu topeni v procentech
        public byte HeatingPercent => (byte)(byte_10 & 0x7F);


        [FieldOffset(11)]
        byte byte_11;

        // :1    YES/NO porucha ventilatoru
        public bool FanFailure => (byte_11 & 0x80) != 0;

        // :1    YES/NO ventilatory prepnute na automaticky regulator vykonu
        public bool IsFanAutoModeEnabled => (byte_11 & 0x40) != 0;

        public ServoStatus BottomInductionServoStatus => (ServoStatus)((byte_11 & 0x38) >> 3);

        public ServoStatus TopInductionServoStatus => (ServoStatus)(byte_11 & 0x07);


        [FieldOffset(12)]
        byte byte_12;

        // :1    YES/NO nizke napeti akumulatoru
        public bool AccumulatorLowVoltage => (byte_12 & 0x80) != 0;

        // :7,    Cislo nalogovaneho ridice
        public byte LoggedDriverId => (byte)(byte_12 & 0x7F);

        [FieldOffset(13)]
        byte byte_13;

        // UNUSED: GsmDummy                 : 1;    // GSM dummy

        // :7    Koncentrace CO2 s dilkem CO2_LSB ppm
        public int Co2Concentration => (byte_13 & 0x7F) * Constants.Co2LSB;


        [FieldOffset(14)]
        byte byte_14;

        // UNUSED: GsmDummy2                : 1;    // GSM dummy2

        // :7, Relativni vlhkost v %
        public byte Humidity => (byte)(byte_14 & 0x7f);


        [FieldOffset(15)]
        byte byte_15;

        // :1    YES/NO topeni 2 prave hori
        public bool IsHeatingFlame2On => (byte_15 & 0x80) != 0;

        // :7,    Tlakova ztrata na filtrech s dilkem AIR_PRESSURE_LSB Pa
        public int AirFiltersPressureLoss => (byte_15 & 0x7F) * Constants.AirPressureLSB;


        [FieldOffset(16)]
        byte byte_16;

        // UNUSED: Dummy3                   : 2;

        // :1    YES/NO dezinfekce je nainstalovana
        public bool IsDisinfectionIsInstalled => (byte_16 & 0x20) != 0;

        // :1    YES/NO dobiji se
        public bool IsChargingOn => (byte_16 & 0x10) != 0;

        // :2    Rezim zobrazeni, kvuli dezinfekci musim propagovat az do TMCR
        public DisplayMode DisplayMode => (DisplayMode)((byte_16 & 0x0C) >> 2);

        // :2    Rezim dezinfekce
        public DisinfectionMode DisinfectionMode => (DisinfectionMode)(byte_16 & 0x03);

        // Teploty zaokrouhlene na 1 stupen
        [FieldOffset(17)]
        public fixed sbyte Temp1[(int)ThermoAddress.Count];

        //public sbyte 

        // Teploty zaokrouhlene na desetinu stupne
        [FieldOffset(23)]
        public fixed short Temp01[(int)ThermoAddress.Count];

        public unsafe short[] GetTemp01Array()
        {
            var list = new List<short>();
            for (var i = 0; i < (int)ThermoAddress.Count; i++)
                list.Add(Temp01[i]);
            return list.ToArray();
        }

        static int TempGet1C(short val)
        {
            // vrati cele stupne
            return Math.Abs(val / 256);
        }

        static int TempGet01C(short val)
        {
            // vrati desetiny stupne 0..9
            return Math.Abs(((Math.Abs(val) & 0xFF) * 10) >> 8);
        }

        public double GetTemperature(ThermoAddress thermoAddress)
        {
            // TEMP_INVALID_01C -1000
            // Neplatna teplota pri zaokrouhleni na desetinu stupne (musi byt do 1024, v zaznamech ukaladam na 10 bitu + znamenko)
            //var val = GetTemp01Array()[(int)thermoAddress];
            //var temp1c = TempGet1C(val);
            //var temp01c = TempGet01C(val);
            //return (double)GetTemp01Array()[(int)thermoAddress] / 10;

            return Temp1[(int)thermoAddress];

        }

        // 4 bajty
        [FieldOffset(35)]
        TransferTime transferTme;

        public DateTime TransferTime => transferTme;

        // Cilova teplota, cele stupne
        [FieldOffset(39)]
        public sbyte TargetTemperature;

        [FieldOffset(40)]
        byte byte_40;

        // :1    YES/NO zda se prave indikuje alarm
        public bool IsAlarmShown => (byte_40 & 0x80) != 0;

        // :7    Maximalni odchylka teploty ve skrini nad cilovou teplotou
        public byte MaxTemperatureOffsetAboveTarget => (byte)(byte_40 & 0x7F);


        [FieldOffset(41)]
        byte byte_41;

        // :1    YES/NO potlaceny reproduktor pri alarmu
        public bool IsAlarmInQuietMode => (byte_41 & 0x80) != 0;

        // :7    Maximalni odchylka teploty ve skrini pod cilovou teplotou
        public byte MaxTemperatureOffsetBelowTarget => (byte)(byte_41 & 0x7F);


        [FieldOffset(42)]
        byte byte_42;

        // :1    Jednotky teploty
        public TemperatureUnits TempUnits => (TemperatureUnits)((byte_42 & 0x80) >> 7);

        // :1    porucha cidla CO2
        public bool Co2Failure => (byte_42 & 0x40) != 0;

        // :1    koncentrace CO2 je prilis vysoka
        public bool IsCo2OverLimit => (byte_42 & 0x20) != 0;

        // :1    tlakova ztrata na vzduchovych filtrech je prilis vysoka
        public bool IsFiltersPressureOverLimit => (byte_42 & 0x10) != 0;

        // :3    Stav ohrevu vody ve spodnim okruhu
        public HeatingCircuitState HeatingCircuitState => (HeatingCircuitState)((byte_42 & 0x0E) >> 1);

        // :1    YES/NO porucha dobijeni
        public bool ChargingFailure => (byte_42 & 0x01) != 0;


        [FieldOffset(43)]
        byte byte_43;

        // :1    Pouze pro GPS: YES/NO Beh motoru vozidla - D+
        public bool IsTruckEngineRunning => (byte_43 & 0x80) != 0;

        // :7    Vykon ventilatoru 0-100%
        public byte FanPower => (byte)(byte_43 & 0x7F);


        [FieldOffset(44)]
        byte dischargePressure;

        // Pouze pro GPS: Tlak vytlaku kompresoru 0-25.5 bar
        public double DischargePressure => (double)dischargePressure / 10;

        [FieldOffset(45)]
        byte byte_45;

        // : 4, Pouze pro GPS: Strida horeni plamene topeni 2 s dilkem LOG_DUTY_CYCLE_LSB
        public int HeatingFlame2DutyCycle => (byte)(byte_45 & 0xF0 >> 4) * Constants.DutyCycleLSB;

        // :4, Pouze pro GPS: Strida horeni plamene topeni 1 s dilkem LOG_DUTY_CYCLE_LSB
        public int HeatingFlame1DutyCycle => (byte_45 & 0x0F) * Constants.DutyCycleLSB;


        [FieldOffset(46)]
        byte byte_46;

        // : 4, Pouze pro GPS: MSB Tlak sani kompresoru 0-6.3 bar
        byte SuctionPressureMsb { get { return (byte)((byte_46 & 0xF0) >> 4); } }


        // :4, Pouze pro GPS: Strida chodu kompresoru s dilkem LOG_DUTY_CYCLE_LSB
        public int CoolingDutyCycle => (byte)(byte_46 & 0x0F) * Constants.DutyCycleLSB;


        [FieldOffset(47)]
        byte byte_47;

        // :6    Pouze pro GPS: Teplota vody na vstupu Webasta ve stupnich Celsia s krokem LOG_HEATING_WATER_TEMP_LSB
        public byte HeatingWaterTemperatureC => (byte)((byte_47 & 0xFC) >> 2);

        //byte SuctionPressureLsb       : 2,    // Pouze pro GPS: LSB Tlak sani kompresoru 0-6.3 bar
        byte SuctionPressureLsb { get { return (byte)(byte_47 & 0x03); } }

        public double SuctionPressure => (double)((SuctionPressureMsb << 2) + SuctionPressureLsb) / 10;


        [FieldOffset(48)]
        byte byte_48;

        // EcuError                 : 1;
        public bool EcuError => (byte_48 & 0x80) != 0;

        //public void SetEcuError() => byte_48 |= 0x80;
        //byte Dummy1                   : 7,

        // Vypln na 52 bajtu, s velikosti 44 bajtu blbla editace cisel v TMCR
        [FieldOffset(49)]
        fixed byte Dummy2[3];
    }
}
