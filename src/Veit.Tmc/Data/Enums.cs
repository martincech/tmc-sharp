﻿namespace Veit.Tmc.Data
{
    public enum ThermoAddress : int
    {
        Front,
        Middle,
        Rear,
        Channel,
        Outside,
        Recirculation,
        Count
    }

    public enum TrollyLayout : byte
    {
        Whole,
        FrontHalf,
        FrontThird,
        RearHalf,
        RearThird,
        MiddleThird,
        Count
    }

    public enum ControlMode : byte
    {
        Off,
        Auto,
        Ventilation,
        Count
    }

    public enum AutoMode : byte
    {
        Heating,
        Cooling
    }

    public enum ServoStatus : byte
    {
        // v poradku, souhlasi pozadovana poloha a zpetne hlaseni
        Ok,
        // nesouhlasi pozadovana poloha a zpetne hlaseni, zatim v casovem limitu
        Running,
        // nesouhlasi pozadovana poloha a zpetne hlaseni, prekrocen cas
        Timeout,
        // ridici napeti (pozadovana poloha) pod limitem (utrzeny potenciometr)
        Control,
        // vypnuto napajeni serva (manualni provoz)
        Off
    }

    public enum FreshAirMode : byte
    {
        // Cerstvy vzduch se reguluje automaticky
        Auto,
        // Cerstvy vzduch se reguluje rucne
        Manual,
        Count
    }

    public enum FlapMode : byte
    {
        /// <summary>
        /// Automatic flap regulation
        /// </summary>
        Auto,
        /// <summary>
        /// Manual flap regulation
        /// </summary>
        Manual,
        Count
    }

    public enum DisinfectionMode : byte
    {
        Off,
        Body,
        Wheels
    }

    /// <summary>
    /// Disinfection automate state
    /// </summary>
    public enum DisinfectionProcessState : byte
    {
        Init,
        WaitForLogo,
        Error,
        Running,
        Finished
    }

    public enum DisplayMode : byte
    {
        // Standardni zobrazeni teplot
        Temperatures,
        // Dezinfekce zahajena
        DisinfectionStarted,
        // Porucha dezinfekce
        DisinfectionError
    }

    public enum HeatingCircuitState : byte
    {
        // Stav po zapnuti rezimu topeni
        Initiated,
        // Topeni je zapnute, cekam na zapaleni plamene
        HeatingOnWaiting,
        // Plamen hori a ceka se na zhasnuti plamene pri dosazeni 85C
        Heating,
        // Plamen zhasnul, po nejakou dobu od zhasnuti kontroluju poruchu topeni (pri poruse plamen take zhasne)
        CheckFailure,
        // Plamen zhasnuty, jede jen cerpadlo a ceka se na pokles teploty vody v okruhu
        PumpOnly,
        // Plamen zhasnuty, cerpadlo odstavene po dobe necinnosti a ceka se na pokles teploty vody v okruhu
        Off,
        Count
    };

    public enum TemperatureUnits : byte
    {
        Celsius,
        Farenheit
    }

    public enum DieselType : byte
    {
        Kubota,
        // Motor Deutz s osazenym cidlem Kubota
        Deutz
    }
}
